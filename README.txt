S5 Presentation Module Suite
----------------------------

The modules in this suite are supposed to help you author and play
S5 presentations. S5 is Eric Meyer's "Simple Standards-Based Slide
Show System", which is based on the Opera Show format. S5
presentations work in a multitude of browsers, and also compatible
with Opera Show.

  http://www.meyerweb.com/eric/tools/s5/

Installation requirements
-------------------------

 - Modules in this folder are compatible with Drupal HEAD

 - S5 itself is not included. You are required to copy the
   contents of the 'ui' folder from your S5 download to modules/s5/ui.
   That means you should have files like modules/s5/ui/default/slides.js

The s5_book module
------------------

This module allows you to play book pages as S5 presentations.
Enabling this module, you will get an 'S5 Presentation' link
by the side of your 'docbook export' and 'opml export' links
on your book pages. This flattens your book structure, so any
hierarchies will be lost. You can use hierarchies however to
structure your presentation properly. Since books only allow
you to distinguish 30 different page weights, if you have
more slides, you need to use hierarchies to maintain proper
ordering. Group your slides by topic, and give your topics
starter slides.

The first slide is searched for metainformation about your
slides, which is injected to further slides. An example first
slide is the following:

<h3 class="s5-author">Name of Author</h3>
<h4 class="s5-company">Company of Author</h4>
<!--p class="s5-presdate">Presentation date</p>
<p class="s5-location">Place, Date and time</p-->

As this example shows, you will have your name and company
displayed on the slide (the slide and thus the presentation
title will be the first node title). Since the presentation
date and location information is in HTML comments, it will
not be displayed. The s5-presdate value is supposed to be
something like 20050920. For more detailed information on
how these values should look like, look into the S5
documentation.

Other slides should contain HTML like:

<ul>
<li>point one</li>
<li>point two</li>
<li>point three</li>
</ul>

See the S5 documentation on what else you can use on the
slides, like handout text, images, incremental display, etc.

Practical usage
---------------

Authoring presentations with the book module might not be
the ideal solution, since it does not allow you to easily
add and remove slides. For this reason, s5_book module
adds an 'add book page' link to the book outline editor
(look into administer >> content >> books). This way,
the outline editor allows you to quickly view, add and
remove slides, as well as reorder them.

Possible later modules
----------------------

Since authoring slides with the book module is still not the
ideal way to look at slideshow editing, there are ideas on
the future direction of supporting S5 authoring. When or
whether these will be implemented at all is still an open
question. Contact the maintainer, if you would like to join
forces.

Credits
-------

Thanks to Eric Meyer for this fantastic slideshow system.
The s5_book module and this readme was written by Gabor Hojtsy <goba@php.net>.
Janos Feher (Aries) helped update the module for Drupal 5.0.0.
